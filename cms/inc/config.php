<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-10-17 16:16:37
 * @Organization: Knockout System Pvt. Ltd.
 */

const DB_HOST = 'localhost';
const DB_USER = 'root';
const DB_PASSWORD = '';
const DB_NAME = 'php_300';

const SITE_URL = "http://localhost/news_3/";
const CMS_URL = SITE_URL."cms/";

const ASSETS_URL = CMS_URL."assets/";
const CSS_URL = ASSETS_URL."css/";
const JS_URL = ASSETS_URL."js/";
const FONT_AWESOME_URL = ASSETS_URL."font-awesome/";

$conn = mysqli_connect('localhost','root', '');
mysqli_select_db($conn, 'php_300');