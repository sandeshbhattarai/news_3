<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-10-17 16:09:12
 * @Organization: Knockout System Pvt. Ltd.
 */
?>

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo JS_URL;?>jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo JS_URL;?>bootstrap.min.js"></script>

</body>

</html>
