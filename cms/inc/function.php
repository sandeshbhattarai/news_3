<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-10-23 15:49:53
 * @Organization: Knockout System Pvt. Ltd.
 */


function debugger($data, $is_die =false){
	echo "<pre>";
	print_r($data);
	echo "</pre>";
	if($is_die){
		exit;
	}
}

function getUserByUsername($username){
	global $conn;
	$sql = "SELECT * FROM users WHERE email = '".$username."' AND status = 1";
	$query = mysqli_query($conn, $sql);
	if(mysqli_num_rows($query) <= 0){
		return false;
	} else {
		return mysqli_fetch_assoc($query);
	}
}