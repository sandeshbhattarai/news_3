<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-10-23 16:06:51
 * @Organization: Knockout System Pvt. Ltd.
 */
if(isset($_SESSION['warning']) && $_SESSION['warning'] != ""){
	echo '<p class="alert-warning">'.$_SESSION['warning'].'</p>';
	unset($_SESSION['warning']);
}

if(isset($_SESSION['error']) && $_SESSION['error'] != ""){
	echo '<p class="alert-danger">'.$_SESSION['error'].'</p>';
	unset($_SESSION['error']);
}

if(isset($_SESSION['success']) && $_SESSION['success'] != ""){
	echo '<p class="alert-success">'.$_SESSION['success'].'</p>';
	unset($_SESSION['success']);
}