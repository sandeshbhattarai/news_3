<?php 
	ob_start();
	
	session_start();
	include 'inc/config.php';
	include 'inc/function.php';

	//debugger($_POST, true);

if(isset($_POST) && !empty($_POST)){
	$username = $_POST['username'];
	$password = sha1($_POST['password']);

	/*User Check*/
	$user_info = getUserByUsername($username);
	if($user_info){
		/*Password*/
		if($password === $user_info['password']){
			$_SESSION['user_id'] = $user_info['id'];
			$_SESSION['full_name'] = $user_info['Name'];
			$_SESSION['role_id'] = $user_info['Role'];
			$_SESSION['email'] = $user_info['email'];

			$_SESSION['success'] = "Welcome to admin panel!";
			@header('location: dashboard.php');
			exit;

		} else {
			$_SESSION['error'] ="Password does not match.";
			@header('location: index.php');
			exit;
		}
		//debugger($user_info, true);
	} else {
		$_SESSION['error'] ="User does not exists.";
		@header('location: index.php');
		exit;
	}
} else {
	$_SESSION['warning'] ="Please Login first.";
	@header('location: index.php');
	exit;
}
ob_flush();